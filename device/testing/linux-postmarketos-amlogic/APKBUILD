# Reference: <https://postmarketos.org/mainline>
# Kernel config based on: arch/arm64/configs/defconfig
# Maintainer: Ferass El Hafidi <vitali64pmemail@protonmail.com>
# Co-Maintainer: exkc <exxxxkc@getgoogleoff.me>
pkgname=linux-postmarketos-amlogic
pkgver=6.2.9
pkgrel=0
pkgdesc="Mainline kernel for Amlogic devices"
arch="aarch64"
_carch="arm64"
_flavor=postmarketos-amlogic
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
"
makedepends="
	bash
	bc
	bison
	devicepkg-dev
	flex
	openssl-dev
	perl
	gmp-dev
	mpc1-dev
	mpfr-dev
	findutils
	postmarketos-installkernel
	gzip
"
_config="config-$_flavor.$arch"
source="
	$pkgname-$pkgver.tar.xz::https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$pkgver.tar.xz
	$_config
	0001-arm64-dts-meson-gxbb-kii-pro-sort-and-tidy-the-dts.patch
	0002-arm64-dts-meson-gxbb-kii-pro-complete-the-bluetooth-node.patch
	0003-arm64-dts-meson-gxbb-kii-pro-add-initial-audio-support.patch
	0004-add-xiaomi-once-support.patch
	0005-add-x96-mini-support.patch
"
builddir="$srcdir/linux-$pkgver"

prepare() {
	default_prepare
	cp -v "$srcdir"/$_config .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		V=1 # See pmaports#1990
}

package() {
	mkdir -p "$pkgdir"/boot
	make install modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs" \
		V=1 # See pmaports#1990

	install -D "$builddir"/include/config/kernel.release \
				"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="
1ea62ac96dec4a11c18b2a2e8bd5ac42b10c63a861f515b8441550fe93702122d73020fec84c5538656e6aea2fbed7966bbb07055f489f4b865ff4c3be806c9f  linux-postmarketos-amlogic-6.2.9.tar.xz
10c6fa8668ff125f377738f6921f1862cd289a4c92a122fd8067201c8c23832efb7c218b44179f00b75a1c0e3118db65945b7ce5a49ca59e38bee0e719bdc95a  config-postmarketos-amlogic.aarch64
3525e0baf74bdab925c2c3b2ce3086effbe463d303f324ac2bafaae5d56422404480ecd8762a9598b9a67fcf6163c872e9ed4c7ba555b927edfb23a8f64ceb0c  0001-arm64-dts-meson-gxbb-kii-pro-sort-and-tidy-the-dts.patch
c8d1aae798623e72248052f0fd07444bf52ee4910326c1de9b8b41a2aefd018636a292d76d7248248bad97730349a8facd32a96b00547cfa3d4ebe0125d4f2ce  0002-arm64-dts-meson-gxbb-kii-pro-complete-the-bluetooth-node.patch
e708b6a14f0ff80c3f25e0323ce3ad8139573de988e72f2f5886ad1bcb40827c4b072188278d14229d468c8a0124f4889fb894ff1fe1e2b3d6db66c3ea7067d4  0003-arm64-dts-meson-gxbb-kii-pro-add-initial-audio-support.patch
9d664c9cc6168a7aa4804662886941744e92c0748e7b7b4fad2472740a4931c89a60cbc4a519913b58933718d02f33f48ff72ea182d912bcd50014ca8d7ff110  0004-add-xiaomi-once-support.patch
3ea569b69297fdb196006b1e1bd82a9dec8d89362ec3ef17918018b6c573143f299a968b8227119660442f6a6e0b795aa4c8a272f6bc23e9622f3fd41ddc97eb  0005-add-x96-mini-support.patch
"
